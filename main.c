#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef enum {
	Z0_BLK_FREE = 0x0,
	Z0_BLK_BUSY = 0x1
} z0_blk_type;

typedef struct {
	uint8_t  type;
	uint8_t	*data;
} z0_blk;

typedef enum {
	Z0_OBJ_FILE = 0x0,
	Z0_OBJ_DIR  = 0x1
} z0_obj_type;

typedef struct z0_obj_s z0_obj;
struct z0_obj_s {
	uint8_t   type; 
	uint32_t  len;
	// TODO: use the free 2 bytes from a pointer.
	uint8_t  *name;

	uint32_t  b_len;
	uint32_t  b_cap;
	uint32_t *b_ids;

	// TODO: implement more memory efficient way
	// 	 of storing files and dirs.
	//uint32_t  i_len;
	//uint32_t  i_cap;
	//z0_obj	 *items;	
};

typedef struct {
	uint32_t  objs_len;
	uint32_t  objs_cap;
	z0_obj   *objs;	

	uint32_t  blk_size;
	uint32_t  blks_len;
	uint32_t  blks_cap;
	z0_blk	 *blks;
} z0_fs;

void
z0_fs_init(z0_fs *fs, uint32_t size) {
	// TODO: implement a better allocator.
	// TODO: add error checking for allocs.
	fs->objs_len = 0;
	fs->objs_cap = 4;
	fs->objs = malloc(sizeof (z0_obj) * fs->objs_cap);

	fs->blk_size = size;
	fs->blks_len = 0;
	fs->blks_cap = 4;
	fs->blks = malloc(sizeof (z0_blk) * fs->blks_cap);
	
	for (uint32_t i = 0; i < fs->blks_cap; ++i) {
		fs->blks[i].type = Z0_BLK_FREE;
		fs->blks[i].data = malloc(sizeof (uint8_t) * size);
	}

#ifdef Z0_DEBUG
	printf("INFO: initialized the file system.\n");
#endif
}

uint32_t
z0_fs_new_file(z0_fs *fs, const char *name) {
	// TODO: implement thread safety.
	if (fs->objs_cap == fs->objs_len) {
		fs->objs_cap *= 2;
		fs->objs = realloc(fs->objs, sizeof (z0_obj) * fs->objs_cap);
	}

	uint32_t id;
	id = fs->objs_len;

	fs->objs[id].type = Z0_OBJ_FILE;
	fs->objs[id].len  = 0;
	// TODO: implement more efficient string library.
	fs->objs[id].name = (uint8_t*)strdup(name);
	fs->objs[id].b_len = 0;
	fs->objs[id].b_cap = 4;
	fs->objs[id].b_ids = malloc(sizeof (uint32_t) * fs->objs[id].b_cap);

#ifdef Z0_DEBUG
	printf("INFO: created a new file with an ID[%u].\n", id);
#endif

	++fs->objs_len;
	return id;
}

void
z0_fs_file_write(z0_fs 	  *fs, 
		 uint32_t  f_id,
		 uint8_t  *data,
		 uint32_t  len) {
	z0_obj *file;
	file = &fs->objs[f_id];
	file->len = len;

	uint32_t blk_count;
	blk_count = len / fs->blk_size + (len % fs->blk_size != 0);	

	if (blk_count >= file->b_cap) {
		uint32_t new_cap;
		new_cap = blk_count;
		// TODO: check if instrinsic for this exists.
		--new_cap;
		new_cap |= new_cap >> 1;
		new_cap |= new_cap >> 2;
		new_cap |= new_cap >> 4;
		new_cap |= new_cap >> 8;
		new_cap |= new_cap >> 16;
		++new_cap;

		file->b_cap = new_cap;
		file->b_ids = realloc(file->b_ids, sizeof (uint32_t) * file->b_cap);
	}
	
	for (uint32_t i = 0; file->b_len < blk_count && i < fs->blks_cap; ++i) {
		if (fs->blks[i].type == Z0_BLK_FREE) {
			file->b_ids[file->b_len] = i;
			fs->blks[i].type = Z0_BLK_BUSY;

			++file->b_len;
			++fs->blks_len;
		}
	}
	
	blk_count -= file->b_len;

	if (fs->blks_len + blk_count >= fs->blks_cap) {
		uint32_t new_cap;
		new_cap = fs->blks_len + blk_count;
		// TODO: check if instrinsic for this exists.
		--new_cap;
		new_cap |= new_cap >> 1;
		new_cap |= new_cap >> 2;
		new_cap |= new_cap >> 4;
		new_cap |= new_cap >> 8;
		new_cap |= new_cap >> 16;
		++new_cap;
	
		fs->blks_cap = new_cap;
		fs->blks = realloc(fs->blks, sizeof (z0_blk) * fs->blks_cap);

		for (uint32_t i = fs->blks_len; i < fs->blks_cap; ++i) {
			fs->blks[i].type = Z0_BLK_FREE;
			fs->blks[i].data = malloc(sizeof (uint8_t) * fs->blk_size);
		}
	}

	while (blk_count--) {
		file->b_ids[file->b_len] = fs->blks_len;

		fs->blks[fs->blks_len].type = Z0_BLK_BUSY;

		++file->b_len;
		++fs->blks_len;
	}

	if (file->b_len == 0) {
#ifdef Z0_DEBUG
		printf("INFO: skipping the memcpy in file with the ID[%u].\n", f_id);
#endif
		return;
	}

	for (uint32_t i = 0; i < file->b_len - 1; ++i) {
#ifdef Z0_DEBUG
		printf("INFO: writing [%u] bytes worth of data in file with the ID[%u] in the block [%u].\n", 
			fs->blk_size, f_id, file->b_ids[i]);
#endif
		memcpy(fs->blks[file->b_ids[i]].data, (data + i * fs->blk_size), fs->blk_size);
	}

	if (file->b_len == 1) {
#ifdef Z0_DEBUG
		printf("INFO: wrote [%u] bytes worth of data in the file with the ID[%u].\n", len, f_id);
		printf("INFO: skipping the last memcpy in file with the ID[%u].\n", f_id);
#endif
		return;
	}

#ifdef Z0_DEBUG
		printf("INFO: writing [%u] bytes worth of data in file with the ID[%u] in the block [%u].\n", 
			len % fs->blk_size, f_id, file->b_ids[file->b_len - 1]);
#endif
	memcpy(fs->blks[file->b_ids[file->b_len - 1]].data, 
	       data + (file->b_len - 1) * fs->blk_size, 
	       len % fs->blk_size);

#ifdef Z0_DEBUG
	printf("INFO: wrote [%u] bytes worth of data in the file with the ID[%u].\n", len, f_id);
#endif
}

void
z0_fs_print(z0_fs *fs) {
	printf("============== File System ================\n");
	printf("OBJECTS LEN: %u\n", fs->objs_len);
	printf("OBJECTS CAP: %u\n", fs->objs_cap);
	printf("BLOCK  SIZE: %u\n", fs->blk_size);
	printf("BLOCKS  LEN: %u\n", fs->blks_len);
	printf("BLOCKS  CAP: %u\n", fs->blks_cap);

	printf("============== Blocks ================\n");
	static const char *b_types[] = { "free", "busy" };
	for (uint32_t i = 0; i < fs->blks_len; ++i) {
		printf("BLOCK ID:   %u\n", i);
		printf("BLOCK TYPE: %s\n", b_types[fs->blks[i].type]);
	}

	printf("============== Objects ================\n");
	static const char *f_types[] = { "file", "dir" };
	for (uint32_t i = 0; i < fs->objs_len; ++i) {
		printf("OBJECT ID:   %u\n", i);
		printf("OBJECT LEN:  %u\n", fs->objs[i].len);
		printf("OBJECT TYPE: %s\n", f_types[fs->objs[i].type]);
		printf("OBJECT NAME: %s\n", fs->objs[i].name);
		printf("OBJECT DATA: ");

		if (fs->objs[i].type == Z0_OBJ_FILE) {
			z0_obj *file;
			file = &fs->objs[i];

			if (file->b_len == 0)
				continue;

			for (uint32_t j = 0; j < file->b_len - 1; ++j)
				printf("%.*s", fs->blk_size, fs->blks[file->b_ids[j]].data);


			if (file->b_len == 1) {
				printf("\n");
				continue;
			}
			
			printf("%.*s\n", file->len % fs->blk_size, 
					 fs->blks[file->b_ids[file->b_len - 1]].data);
		}
	}
}

void
z0_fs_sync(z0_fs *fs) {
	FILE *meta;
	meta = fopen("meta.vfs", "w");
	if (meta != NULL) {
		fwrite(fs, sizeof (z0_fs), 1, meta);
		for (uint32_t o = 0; o < fs->objs_len; ++o) {
			fwrite(&fs->objs[o], sizeof (z0_obj), 1, meta);
			// TODO: seriously write a custom string library.
			fwrite(fs->objs[o].name, sizeof (uint8_t), strlen((const char*)fs->objs[o].name), meta);
			fwrite(fs->objs[o].b_ids, sizeof (uint32_t), fs->objs[o].b_cap, meta);
		}
	
		for (uint32_t b = 0; b < fs->blks_cap; ++b) {
			fwrite(&fs->blks[b], sizeof (z0_blk), 1, meta);
			fwrite(fs->blks[b].data, sizeof (uint8_t), fs->blk_size, meta);
		}
		fclose(meta);
	}
}

void
z0_fs_mount(z0_fs *fs) {
	FILE *meta;
	meta = fopen("meta.vfs", "r");

	if (meta != NULL) {
		fread(fs, sizeof (z0_fs), 1, meta);
		for (uint32_t o = 0; o < fs->objs_len; ++o) {
			fread(&fs->objs[o], sizeof (z0_obj), 1, meta);
			// TODO: seriously write a custom string library.
			fread(fs->objs[o].name, sizeof (uint8_t), strlen((const char*)fs->objs[o].name), meta);
			fread(fs->objs[o].b_ids, sizeof (uint32_t), fs->objs[o].b_cap, meta);
		}
	
		for (uint32_t b = 0; b < fs->blks_cap; ++b) {
			fread(&fs->blks[b], sizeof (z0_blk), 1, meta);
			fread(fs->blks[b].data, sizeof (uint8_t), fs->blk_size, meta);
		}
		fclose(meta);
	}	
}

void
z0_fs_free(z0_fs *fs) {
	for (uint32_t i = 0; i < fs->blks_cap; ++i)
		free(fs->blks[i].data);
	free(fs->blks);

	for (uint32_t i = 0; i < fs->objs_len; ++i) {
		free(fs->objs[i].b_ids);
		free(fs->objs[i].name);
	}
	free(fs->objs);	
}

int
main() {
	z0_fs fs;
	//z0_fs_init(&fs, 16);

	//uint32_t file_1;
	//file_1 = z0_fs_new_file(&fs, "file_1.txt");

	//char text_1[] = "lorem impsum don't remember what comes after that.";
	//z0_fs_file_write(&fs, file_1, text_1, sizeof (text_1));

	//uint32_t file_2;
	//file_2 = z0_fs_new_file(&fs, "file_2.json");

	//char text_2[] = "{ name: John, age: 32, gender: male, status: married, profession: sanitor, hobbies: hiking }";
	//z0_fs_file_write(&fs, file_2, text_2, sizeof (text_2));

	z0_fs_mount(&fs);
	z0_fs_print(&fs);
	//z0_fs_sync(&fs);

	z0_fs_free(&fs);
	return 0;
}
