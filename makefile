SRC=main.c
OUT=vfs.out

all:
	gcc $(SRC) -g -DZ0_DEBUG -Wall -Wextra -fsanitize=leak -fsanitize=address -o $(OUT)
