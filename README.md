# 0xVFS

A virtual file system written in `C`.

### API

- To create a file system you first need to initialize the `z0_fs` structure.

```c
z0_fs fs;
// 16 inidicates the block size. Picking a number which is power of 2 is recommended.
z0_fs_init(&fs, 16); 

// ...

z0_fs_free(&fs);
```

- To sync the data you can use the `z0_fs_sync` function which will create a `meta.vfs` file.

```c
z0_fs_sync(&fs);
```

- To mount the meta file you can use the `z0_fs_mount` function.

```c
z0_fs_mount(&fs);
```
